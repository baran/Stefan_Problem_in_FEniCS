__author__ = "Bjoern Baran <baran@mpi-magdeburg.mpg.de>"
__date__ = "2015-07-22"
__copyright__ = "Copyright (C) 2015 " + __author__
__license__  = ""

from dolfin import *

class Problem():
    def __init__(self):
        #################################################################################
        ## Create mesh
        #################################################################################
        self.mesh = Mesh("./stefan_mesh.xml")
        
        #################################################################################
        ## Define function spaces
        #################################################################################
        self.V = VectorFunctionSpace(self.mesh, "Lagrange", 2)
        self.Q = FunctionSpace(self.mesh, "Lagrange", 1)
        self.W = VectorFunctionSpace(self.mesh, 'Lagrange', 1)
        self.B = FunctionSpace(self.mesh, 'CG', 1) # boundary node marking
        self.V0 = FunctionSpace(self.mesh, 'DG', 0)
        
        #################################################################################
        ## Define trial and test functions
        #################################################################################
        # Heat equations
        self.T = TrialFunction(self.Q) # temperature
        self.w = TestFunction(self.Q)
        # Mesh movement
        self.V_var = TrialFunction(self.Q) # interface velocity
        self.V_all = TrialFunction(self.W) # interface velocity extended to whole mesh
        self.v_v = TestFunction(self.W)
        self.r = TestFunction(self.Q)
        # Navier-Stokes
        self.p = TrialFunction(self.Q) # preasure
        self.u = TrialFunction(self.V) # velocity
        self.v = TestFunction(self.V)
        self.q = TestFunction(self.Q)
        
        #################################################################################
        ## Create functions
        #################################################################################
        # Heat equations
        self.T0 = Expression('(4 * x[1] - 0.66666) * 31', degree=1) # initial temperature
        self.T0 = interpolate(self.T0, self.Q)
        # Mesh movement
        self.V_int = Function(self.W) # interface velocity
        self.normals = Function(self.W) # interface normals
        self.boundary_nodes = Function(self.B) # mark all nodes on boundary
        # interface normal
        # Navier-Stokes
        self.u0 = Function(self.V) # initial velocity
        self.u1 = Function(self.V)
        self.p0 = Function(self.Q) # initial preasure
        self.p1 = Function(self.Q)
        
        #################################################################################
        ## Set parameter values
        #################################################################################
        self.dt = 0.05 # time step
        self.t_end = 15 # time horizon
        self.ks = Constant(1) # thermal conductivity of solid
        self.kl = Constant(0.6) # thermal conductivity of liquid
        self.alpha = conditional(gt(self.T0, 0), self.kl, self.ks)
        self.La = Constant(336) # latent heat
        self.t_min = -54 # minimum temperature
        self.t_max = 108 # maximum temperature 
        self.T_heat = Constant(self.t_max) # temperature of the warming canal
        self.T_cool = Constant(self.t_min) # temperature of the freezing canal
        self.delta = 1e-3 # distance from zero level in which nodes are on the interface
        self.zero = Function(self.W)
        self.mesh_movement_distance = 1 # distance from interface in which mesh movement 
        # will be extended
        self.p_in = Constant(1.5) # preasure at inflow
        self.k = Constant(self.dt) # time step
        self.k = interpolate(self.k, self.V0)
        self.f = Constant((0, 0))
        self.nu = Constant(0.1)
        self.nu = interpolate(self.nu, self.V0)
        
        self.beta = Constant(self.dt * 30) # what is this good for??
        self.beta = interpolate(self.beta, self.V0)
        
        #################################################################################
        ## Define boundary conditions
        #################################################################################
        def BCheat(x, on_boundary): # warming canal and inflow
            return on_boundary and (x[0] < DOLFIN_EPS) and (x[1] > .6) and (x[1] < .8)
        self.BCheat = BCheat 
        def BCoutflow(x, on_boundary): # outflow
            return on_boundary and (x[0] > 1 - DOLFIN_EPS) and (x[1] > .2) \
            and (x[1] < .4) and (self.T(x) > 0 - DOLFIN_EPS)#(not BCslip(x, on_boundary))#(self.T(x) > 0 - DOLFIN_EPS)
        self.BCoutflow = BCoutflow
        def BCcool(x, on_boundary): # freezing canal
            return on_boundary and (x[1] < DOLFIN_EPS)
        self.BCcool = BCcool
        class Interface(SubDomain): # interface between solid and liqid phase
            def inside(self_, x, on_boundary):
                return (self.T0(x) > -self.delta) and (self.T0(x) < self.delta)
        self.Interface = Interface
        def BCb(x, on_boundary): # region in which mesh movement will be expanded to
            return (self.y(x) > self.mesh_movement_distance - DOLFIN_EPS) or (on_boundary and \
            ((x[1] < DOLFIN_EPS)) or (x[1] > 1 - DOLFIN_EPS))
        self.BCb = BCb            
        def BCslip(x, on_boundary): # no slip for Navier Stokes at interface
            return (self.T(x) < 0 + self.delta)# + DOLFIN_EPS)
        self.BCslip = BCslip
        def BCrest(x, on_boundary): # no slip for Navier Stokes at outer boundary
            return on_boundary and (not BCheat(x, on_boundary)) and \
            (not BCoutflow(x, on_boundary)) 
        self.BCrest = BCrest
        # mark nodes and facets on the interface
        self.node_markers = MeshFunction("size_t", self.mesh, self.mesh.topology().dim() - 2)
        self.facet_markers = MeshFunction("size_t", self.mesh, self.mesh.topology().dim() - 1)
        self.facet_markers.set_all(0)
        self.node_markers.set_all(0)
        self.Interf = Interface()
        self.Interf.mark(self.facet_markers, 1)
        self.Interf.mark(self.node_markers, 1)
        # mark  boundary nodes
        # Mark a CG1 Function with ones on the boundary
        self.bcb = DirichletBC(self.B, 1, DomainBoundary())
        self.bcb.apply(self.boundary_nodes.vector())
        # velocity of interface for extended velocity
        self.bc_interface_v = DirichletBC(self.W, self.V_int, self.facet_markers, 1)
        self.bc_outside_expand_region = DirichletBC(self.W, (0., 0.), self.BCb)
        # Get vertices sitting on boundary
        self.dof_2_vertex = dof_to_vertex_map(self.B)
        self.nodes_on_boundary = self.dof_2_vertex[self.boundary_nodes.vector() == 1.0]
        for node_idx in self.nodes_on_boundary:
            if self.node_markers.array()[node_idx] == 1:
                self.node_markers.array()[node_idx] = 2
        # bounday conditions for heat equation
        self.bc_heat = DirichletBC(self.Q, self.T_heat, self.BCheat)
        self.bc_cool = DirichletBC(self.Q, self.T_cool, self.BCcool)
        self.bc_interface = DirichletBC(self.Q, 0, self.facet_markers, 1)
        self.bcs_temp = [self.bc_heat, self.bc_cool, self.bc_interface]
        # boundary conditions for Navier Stokes equation
        self.noslip_interface  = DirichletBC(self.V, (0, 0),self.BCslip)
        self.noslip= DirichletBC(self.V, (0, 0), self.BCrest)
        self.inflow  = DirichletBC(self.Q, self.p_in, BCheat)
        self.outflow = DirichletBC(self.Q, 0, self.BCoutflow)
        self.bc_velocity = [self.noslip, self.noslip_interface]
        self.bc_preasure = [self.inflow, self.outflow]