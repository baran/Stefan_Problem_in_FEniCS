# -*- coding: utf-8 -*-
"""

Created on Sat Dec  5 14:32:40 2015

@author: baran
"""

from dolfin import *
from scipy import load, linspace, ones

class Settings():
    def __init__(self, problem, p_in):
        problem.plot_level = 0
        problem.save_data = 0
        
        problem.dt = 0.01 # time step
        problem.t_end = 1 # time horizon
        problem.t = linspace(0, problem.t_end, round(problem.t_end / problem.dt) + 1)
        problem.ks = Constant(1) # thermal conductivity of solid
        #problem.ks = Constant(0.2)
        problem.kl = Constant(0.6) # thermal conductivity of liquid
        problem.alpha = conditional(gt(problem.T0, 0), problem.kl, problem.ks)
#        problem.La = Constant(336) # latent heat
        problem.La = Constant(150)
        problem.t_min = -1. # minimum temperature
        problem.t_max = 4 # maximum temperature 
        problem.T_heat = Constant(problem.t_max) # temperature of the warming canal
        #problem.T_heat = Expression("sin(4.0*t) + 4", t=0.0)
        #problem.t = 0
        #problem.T_heat = conditional(gt(t, 1.5), 4, 1)
        problem.T_cool = Constant(problem.t_min) # temperature of the freezing canal
        #problem.T_cool = Expression('-4 * pow(x[0], 2) - 4')
        problem.T_cool = Expression('1. * (-.6 - 0 * cos((x[0] - 0.07) * 1.7 * pi) * exp((x[0] - 1) * 2) + 0 * x[0])',degree=1)
        problem.delta = 0.01 # distance from zero level in which nodes are on the interface
        problem.zero = Function(problem.W)
        problem.mesh_movement_distance = 1 # distance from interface in which mesh movement 
        # will be extended
        if len(p_in) == 0:
            problem.p_in = 2 # preasure at inflow
            problem.p_in = ones(problem.t.shape[0]) * problem.p_in
#            problem.p_in[0 : 17] = 20
#            problem.p_in[65 : 90] = 20
#            problem.p_in[80 : ] = 20 
        else:
            problem.p_in = p_in

#        problem.p_in = sin(problem.t * 3) * problem.p_in
#        print(problem.p_in)
        problem.k = Constant(problem.dt) # time step
        problem.k = interpolate(problem.k, problem.V0) # time step
        problem.f = Constant((0, 0))
#        problem.eta = Constant(0.1)
        RE = 20.
        problem.eta = 1. / RE
        problem.eta_count = 0
        problem.p_min = 0
        problem.p_max = 20
#        problem.eta = interpolate(problem.eta, problem.V0)
        
#        problem.beta = Constant(1)
#        problem.beta = interpolate(problem.beta, problem.V0)
        
        problem.interface_desired = load("Problems/settings_snapshots/h_mat_move1.npy")
        problem.Lambda = 1e+6 # weight of distance at end in cost function
        problem.Lambda2 = 1e-20#1e+6 # weight of distance in cost function
        problem.lambd = .1e-9 # weight of control cost in cost function
        problem.i = 0 # iteration counter
