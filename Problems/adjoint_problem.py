# -*- coding: utf-8 -*-
"""
Data for adjoint (backward) problem

Created on Wed Nov 25 14:07:50 2015

@author: baran
"""
from dolfin import *

class Problem():
    def __init__(self, problem):
        self.problem = problem # forward problem
        #########################################################################
        ## Load meshes
        #########################################################################
        self.mesh = problem.mesh
        self.boundary_mesh = problem.boundary_mesh
        self.mesh_l = problem.mesh_l
        
        #########################################################################
        ## Define function spaces
        #########################################################################
        self.CG1 = FunctionSpace(self.mesh, 'CG', 1)
        self.CG2 = FunctionSpace(self.mesh, 'CG', 2)
        self.DG0 = FunctionSpace(self.mesh, 'DG', 0)
        self.VCG1 = VectorFunctionSpace(self.mesh, 'CG', 1)
        self.VCG2 = VectorFunctionSpace(self.mesh, 'CG', 2)
        self.CG1_boundary = FunctionSpace(self.boundary_mesh, 'CG', 1)
        self.CG2_boundary = FunctionSpace(self.boundary_mesh, 'CG', 2)
        self.VCG1_l = VectorFunctionSpace(self.mesh_l, 'CG', 1)
#        self.VCG2_l = VectorFunctionSpace(self.mesh_l, 'CG', 2)
        self.CG1_l = FunctionSpace(self.mesh_l, 'CG', 1)
        self.CG2_l = FunctionSpace(self.mesh_l, 'CG', 2)
        P2 = VectorElement("Lagrange", self.mesh_l.ufl_cell(), 2) # velocity
        P1 = FiniteElement("Lagrange", self.mesh_l.ufl_cell(), 1) # preasure
#        self.M = self.VCG2_l * self.CG1_l
        TH = P2 * P1
        self.M = FunctionSpace(problem.mesh_l, TH)
        
        #########################################################################
        ## Load mesh functions
        #########################################################################
        self.facet_markers = problem.facet_markers
        self.node_markers = problem.node_markers
        
        #########################################################################
        ## Set parameter values
        #########################################################################
        self.i = problem.i
        self.La = problem.La
        self.dt = problem.dt
        self.Lambda = problem.Lambda
        self.Lambda2 = problem.Lambda2
        self.lambd = problem.lambd
        self.delta = problem.delta
        self.eta = problem.eta
        
        
        #########################################################################
        ## Load functions
        #########################################################################
        self.T = problem.T
        self.V_all = problem.V_all
        self.V_all_l = problem.V_all_l
        self.u = problem.u1_
        self.V_all_back = Function(self.V_all.function_space())
        self.V_all_back.vector()[:] = self.V_all.vector().array() * -1.
        self.h = problem.h
        self.h_x = problem.h_x
        self.h_d = problem.h_d
        self.jump_y = problem.jump_y
        self.jump_y2 = problem.jump_y2
        self.alpha = conditional(gt(self.T, 0), problem.kl, problem.ks)
        
        #########################################################################
        ## Load boundary conditions
        #########################################################################
        self.BCheat = problem.BCheat
        self.BCcool = problem.BCcool
        self.BCoutflow = problem.BCoutflow
        self.BCslip = problem.BCslip
        self.BCrest = problem.BCrest
        class Liquid(SubDomain):
            def inside(self_, x, on_boundary):
                return self.T(x) > 0 - self.delta
        self.liquid = Liquid()
        
    def previous_time_step(self):
        self.i = self.i - 1
        
        #########################################################################
        ## Move meshes
        #########################################################################
        
        ALE.move(self.mesh, self.V_all_back)
        self.mesh.bounding_box_tree().build(self.mesh)
        ALE.move(self.mesh_l, SubMesh(self.mesh, self.liquid))
        self.mesh_l.bounding_box_tree().build(self.mesh_l)
        
        #########################################################################
        ## Set function values
        #########################################################################
        self.T.vector().set_local(self.problem.T_mat[ : , self.i])
        self.V_all.vector().set_local(self.problem.V_all_mat[ : , self.i])
        self.V_all_l.vector().set_local(self.problem.V_all_l_mat[ : , self.i])
        self.u.vector().set_local(self.problem.u_mat[ : , self.i])
        self.V_all_back.vector()[:] = self.V_all.vector().array() * -1.
        self.h.vector().set_local(self.problem.h_mat[ : , self.i])
        self.h_x.vector().set_local(self.problem.h_x_mat[ : , self.i])
        self.jump_y.vector().set_local(self.problem.jump_y_mat[ : , self.i])
        self.jump_y2.vector().set_local(self.problem.jump_y2_mat[ : , self.i])
        
    def reset(self, problem):
        self.__init__(problem)    