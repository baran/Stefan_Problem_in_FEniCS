__author__ = "Bjoern Baran <baran@mpi-magdeburg.mpg.de>"
__date__ = "2015-07-22"
__copyright__ = "Copyright (C) 2015 " + __author__
__license__  = ""

# List of available Problems
problems = ["default_problem", "para_test"]
from Problems.settings import Settings

# Wrapper for different solvers
def Problem(name, problem):
    "Return the problem data for the given problem name"
    exec("from Problems.%s import Problem as requested_problem" % name, globals())
    return requested_problem(problem)