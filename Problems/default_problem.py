__author__ = "Bjoern Baran <baran@mpi-magdeburg.mpg.de>"
__date__ = "2015-07-22"
__copyright__ = "Copyright (C) 2015 " + __author__
__license__  = ""

from dolfin import *
from Problems import Settings

class Problem():
    def __init__(self, p_in):
        #################################################################################
        ## Create mesh
        #################################################################################
        self.mesh = Mesh("./stefan_mesh.xml")
        
        #################################################################################
        ## Define function spaces
        #################################################################################
        self.V = VectorFunctionSpace(self.mesh, "Lagrange", 2) # velocity
        self.Q = FunctionSpace(self.mesh, "Lagrange", 1) # temperature and preasure
        self.CG2 = FunctionSpace(self.mesh, "Lagrange", 2) # temperature
        self.W = VectorFunctionSpace(self.mesh, 'Lagrange', 1) # mesh velocity
        self.B = FunctionSpace(self.mesh, 'CG', 1) # boundary node marking
        self.V0 = FunctionSpace(self.mesh, 'DG', 0) # for Constants
        
        #################################################################################
        ## Define trial and test functions
        #################################################################################
        # Heat equations
        self.T = TrialFunction(self.CG2) # temperature
        self.w = TestFunction(self.CG2)
        # Mesh movement
        self.V_var = TrialFunction(self.Q) # interface velocity
        self.V_all = TrialFunction(self.W) # interface velocity extended to whole mesh
        self.v_v = TestFunction(self.W)
        self.r = TestFunction(self.Q)
        # Navier-Stokes
        self.p = TrialFunction(self.Q) # preasure
        self.u = TrialFunction(self.V) # velocity
        self.v = TestFunction(self.V)
        self.q = TestFunction(self.Q)
        
        #################################################################################
        ## Create functions
        #################################################################################
        # Heat equations
        self.T0 = Expression('4 * x[1] - 0.66666', degree=2) # initial temperature
        self.T0 = interpolate(self.T0, self.CG2)
        # Mesh movement
        self.V_int = Function(self.W) # interface velocity
        self.normals = Function(self.W) # interface normals
        self.boundary_nodes = Function(self.B) # mark all nodes on boundary
        # Navier-Stokes
        self.u0 = Function(self.W) # initial velocity
        self.u1 = Function(self.W)
        self.u1_ = Function(self.W)
        self.p0 = Function(self.Q) # initial preasure
        self.p1 = Function(self.Q)
        self.p1_ = Function(self.Q)
        
        #################################################################################
        ## Set parameter values
        #################################################################################
        Settings(self, p_in)
        
        #################################################################################
        ## Define boundary conditions
        #################################################################################
        def BCheat(x, on_boundary): # warming canal and inflow
            return on_boundary and (x[0] < DOLFIN_EPS) and (x[1] > .6) and (x[1] < .8)
        self.BCheat = BCheat 
        def BCoutflow(x, on_boundary): # outflow
            return on_boundary and (x[0] > 1 - DOLFIN_EPS) and (x[1] > .2) \
            and (x[1] < .4) and (self.T(x) > 0 - DOLFIN_EPS)
        self.BCoutflow = BCoutflow
        def BCcool(x, on_boundary): # freezing canal
            return on_boundary and (x[1] < DOLFIN_EPS)# and (x[0] < 0.2)
        self.BCcool = BCcool
        class Interface(SubDomain): # interface between solid and liqid phase
            def inside(self_, x, on_boundary):
                return (self.T0(x) > -self.delta) and (self.T0(x) < self.delta)
        self.Interface = Interface
        def BCb(x, on_boundary): # region in which mesh movement will be expanded to
            return (self.y(x) > self.mesh_movement_distance - DOLFIN_EPS) or (on_boundary and \
            ((x[1] < DOLFIN_EPS)) or (x[1] > 1 - DOLFIN_EPS))
        self.BCb = BCb            
        def BCslip(x, on_boundary): # no slip for Navier Stokes at interface
            return (self.T(x) < 0 + self.delta) and (self.T(x) > 0 - self.delta)
        self.BCslip = BCslip
        def BCrest(x, on_boundary): # no slip for Navier Stokes at outer boundary
            return on_boundary and (not BCheat(x, on_boundary)) and \
            (not BCoutflow(x, on_boundary)) 
        self.BCrest = BCrest
        # mark nodes and facets on the interface
        self.node_markers = MeshFunction("size_t", self.mesh, self.mesh.topology().dim() - 2)
        self.facet_markers = FacetFunction("size_t", self.mesh)
        self.facet_markers.set_all(0)
        self.node_markers.set_all(0)
        self.Interf = Interface()
        self.Interf.mark(self.facet_markers, 1)
        self.Interf.mark(self.node_markers, 1)
        # mark  boundary nodes
        # Mark a CG1 Function with ones on the boundary
        self.bcb = DirichletBC(self.B, 1, DomainBoundary())
        self.bcb.apply(self.boundary_nodes.vector())
        # velocity of interface for extended velocity
        self.bc_interface_v = DirichletBC(self.W, self.V_int, self.facet_markers, 1)
        self.bc_outside_expand_region = DirichletBC(self.W, (0., 0.), self.BCb)
        # Get vertices sitting on boundary
        self.dof_2_vertex = dof_to_vertex_map(self.B)
        self.nodes_on_boundary = self.dof_2_vertex[self.boundary_nodes.vector() == 1.0]
        for node_idx in self.nodes_on_boundary:
            if self.node_markers.array()[node_idx] == 1:
                self.node_markers.array()[node_idx] = 2
        # bounday conditions for heat equation
        self.bc_heat = DirichletBC(self.CG2, self.T_heat, self.BCheat)
        self.bc_cool = DirichletBC(self.CG2, self.T_cool, self.BCcool)
        self.bc_interface = DirichletBC(self.CG2, 0, self.facet_markers, 1)
        self.bcs_temp = [self.bc_heat, self.bc_cool, self.bc_interface]
        # boundary conditions for Navier Stokes equation
        self.noslip_interface  = DirichletBC(self.V, (0, 0),self.BCslip)
        self.noslip= DirichletBC(self.V, (0, 0), self.BCrest)
    def reset(self, p_in):
        self.__init__(p_in)