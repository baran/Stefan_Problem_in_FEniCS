# -*- coding: utf-8 -*-
"""
Set up Stefan Problem with mesh movement

Created on Mon Jun  8 15:31:09 2015

@author: baran
"""

#################################################################################
## Import required functions
#################################################################################
from dolfin import *
from helpers import interface_distance
from Problems import Problem
from Solvers import Solver
from cost_function.cost_function import Cost_Function
from matplotlib.pyplot import figure, show, pause, close
set_log_level(30)

class Stefan_problem_forward():
    def __init__(self, p_in):
        
        #################################################################################
        ## Load problem data
        #################################################################################
        self.problem = Problem("default_problem", p_in)
        
        #################################################################################
        ## Settings for Script
        #################################################################################
        self.plot_level = self.problem.plot_level
        self.save_data = 1#self.problem.save_data
        
        #################################################################################
        ## Initialize Solvers
        #################################################################################
        # Navier Stokes 
        self.solver_NSE = Solver("Newton_NSE", self.problem)
        #solver_NSE = Solver("Chonin", problem)
        # mesh movement
        self.solver_mesh_movement = Solver("mesh_movement", self.problem)
        # heat equations
        self.solver_temperature = Solver("heat", self.problem)
        # interface velocity
        self.solver_interface = Solver("interface", self.problem)
        # interface position
        self.solver_h = Solver("graph", self.problem)
        # interface distance
        self.cost = Cost_Function(self.problem, self.solver_h)
        # desired interface position
        self.solver_h_d = Solver("graph_desired", self.problem)
        # compute a distance function to the interface
        self.problem.y = interface_distance(self.problem.mesh, self.problem.facet_markers)
        # Jump function
        self.solver_jump = Solver("jump", self.problem)
        
        #################################################################################
        ## Initialize plots
        #################################################################################
        if self.plot_level:
            figure_count = 1
            # plot
            if self.plot_level > 1:
                figure(figure_count)
                plot(self.problem.p0, title="Pressure at   t = %1.2f s" % self.problem.t[0], rescale=True)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                plot(self.problem.u0, title="Velocity at   t = %1.2f s" % self.problem.t[0], rescale=True)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                plot(self.problem.T, title='temperature at t = 0')
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                self.pm = plot(self.problem.facet_markers, title='interface facets')
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                plot(self.problem.node_markers, title='interface vertices')
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                self.pm.set_min_max(0, 1)
            p_graph_desired = plot(self.problem.h_d, title='desired interface position')
            show(block=False)
            pause(1e-4)
            figure_count += 1
            figure(figure_count)
#            p_graph_desired.set_min_max(0.15, 0.3)
            p_graph_desired.plot(self.problem.h_d)
            show(block=False)
            pause(1e-4)
            figure_count += 1
            figure(figure_count)
            self.p_graph = plot(self.problem.h, title='interface graph h')
            show(block=False)
            pause(1e-4)
#            self.p_graph.set_min_max(0.15, 0.3)
        
        #################################################################################
        ## Initialize files 
        #################################################################################
        if self.save_data:
            self.tfile = File("results/temperature.pvd")
            self.vfile = File("results/velocity.pvd")
            self.int_vfile = File("results/interface.pvd")
            self.mesh_vfile = File("results/mesh_movement.pvd")
            self.pfile = File("results/preasure.pvd")
        
    def solve(self):
        #################################################################################
        ## Start iteration
        #################################################################################
        for self.problem.i in range(1, self.problem.t.size):
            if self.plot_level:
                print('t = %1.2f s' % self.problem.t[self.problem.i])
            #############################################################################
            ## Compute interface velocity and move the mesh
            #############################################################################
            self.solver_interface.solve(self.problem)
            if self.plot_level:
                figure_count = 1
                # plot interface velocity
                figure(figure_count)
                plot(self.problem.V_int, scale=1., title='interface movement')
                show(block=False)
                pause(1e-4)
                figure_count += 1
                if self.plot_level > 1:
                    figure(figure_count)
                    plot(self.problem.normals, title='normals')
                    show(block=False)
                    pause(1e-4)
                    figure_count += 1
            # Compute expansion
            self.solver_mesh_movement.solve(self.problem)
            if self.plot_level > 1:
                # plot expansion
                figure(figure_count)
                plot(self.problem.V_all, title='mesh movement')
                show(block=False)
                pause(1e-4)
                figure_count += 1
            # move nodes
            ALE.move(self.problem.mesh, self.problem.V_all)
            self.problem.mesh.bounding_box_tree().build(self.problem.mesh) # Tree needs to be rebuid!!!
            
            #############################################################################
            ## Compute velocity of the liquid and preasure for next timestep
            #############################################################################
            self.solver_NSE.solve(self.problem)
            
            #############################################################################
            ## Compute temperature for next timestep
            #############################################################################
            self.solver_temperature.solve(self.problem)
            if self.plot_level > 1:
                # plot temperature and moved mesh
                figure(figure_count)
                temp = plot(self.problem.T, title='temperature at t = %1.2f s' % self.problem.t[self.problem.i])
                temp.set_min_max(self.problem.t_min, self.problem.t_max)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                self.pm.plot(self.problem.facet_markers)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                plot(self.problem.node_markers, title='interface vertices')
                show(block=False)
                pause(1e-4)
                figure_count += 1
            
            #############################################################################
            ## Update interface graph h
            ############################################################################# 
            self.solver_h.update(self.problem)
            self.solver_h_d.update(self.problem)
            if self.plot_level:
                figure(figure_count)
                p_graph_desired = plot(self.problem.h_d, title='desired interface position')
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                p_graph_desired.plot(self.problem.h_d)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                self.p_graph.plot(self.problem.h)
                show(block=False)
                pause(1e-4)
                figure_count += 1
            
            #############################################################################
            ## Evaluate cost functional
            #############################################################################             
            # interface distance to desired interface
            self.cost_value = self.cost.eval(self.problem)
            if self.plot_level:
                print("Cost function: %e" % self.cost_value)
            
            #############################################################################
            ## Compute jump of temperature gradient in y direction
            #############################################################################
            self.solver_jump.solve(self.problem)
            
            #############################################################################
            ## Move to next time step
            #############################################################################
            self.problem.T0.assign(self.problem.T)
            self.problem.u0.assign(self.problem.u1_)
            self.problem.p0.assign(self.problem.p1_)
            if self.plot_level > 1:
                figure(figure_count)
                plot(self.problem.p0, title="Pressure at   t = %1.2f s" % self.problem.t[self.problem.i], rescale=True)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                plot(self.problem.u0, title="Velocity at   t = %1.2f s" % self.problem.t[self.problem.i], rescale=True)
                show(block=False)
                pause(1e-4)
            if not (self.problem.i % 10):
                close('all')
            
            #############################################################################
            ## Save results
            #############################################################################
            if self.save_data:
                self.tfile << self.problem.T
                self.vfile << self.problem.u0
                self.int_vfile << self.problem.V_int
                self.mesh_vfile << self.problem.V_all
                self.pfile << self.problem.p0
        
        #################################################################################
        ## End
        #################################################################################
        print("Cost function: %e" % self.cost_value)
        print("\t Distance: %e" % self.cost.dist.eval(self.problem))
        print("\t Distance(all): %e" % self.cost.d2.sum())
    def reset(self, p_in):
        self.__init__(p_in)