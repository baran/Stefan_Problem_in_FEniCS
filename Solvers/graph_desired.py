# -*- coding: utf-8 -*-
"""
Update desired graph

Created on Sat Dec  5 15:27:32 2015

@author: baran
"""

class Solver():
    def __init__(self, problem):
        problem.h_d.vector().set_local(problem.interface_desired[ : , 0])
        
    def update(self, problem):
        problem.h_d.vector().set_local(problem.interface_desired[ : , problem.i])