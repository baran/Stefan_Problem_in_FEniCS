# -*- coding: utf-8 -*-
"""
Solver for the adjoint equation on the interface mapped to the bottom

Created on Wed Nov 25 15:44:03 2015

@author: baran
"""

from dolfin import *

class Solver():
    "Compute psi the adjoint state on the interface mapped to the bottom boundary."
    "Works on the boundary mesh already constructed in the forward computations"
    
    def __init__(self, adjoint):
        # Define functions
        self.psi_ = TrialFunction(adjoint.CG2_boundary)
        self.v = TestFunction(adjoint.CG2_boundary)
        adjoint.psi0 = Function(adjoint.CG2_boundary)
        adjoint.psi_int = Function(adjoint.CG1)
        
        # Get initial condition
        a = inner(self.psi_, self.v) * dx
        L = -1. *  adjoint.Lambda / adjoint.La * (adjoint.h - adjoint.h_d) * self.v * dx
        solve(a == L, adjoint.psi0)
        
        # Define boundary conditions
        def BC(x, on_boundary):
            return on_boundary
        self.bc = [DirichletBC(adjoint.CG2_boundary, 0, BC)]
        
        # Create bilinear and linear forms
        self.a1 = adjoint.La / adjoint.dt * self.psi_ * self.v * dx
        self.a2 = Dx(2. * adjoint.h_x * adjoint.jump_y * self.psi_ , 0) * self.v * dx
        self.a3 = -1. * (1. + adjoint.h_x ** 2) * adjoint.jump_y2 * self.psi_ * self.v * dx
        self.a = self.a1 + self.a2 + self.a3
        self.L = adjoint.La / adjoint.dt * adjoint.psi0 * self.v * dx \
            - adjoint.Lambda2 * (adjoint.h - adjoint.h_d) * self.v * dx 
        
        # initialize psi
        adjoint.psi = Function(adjoint.CG2_boundary)
        
        # Solver
        self.solver = LinearSolver()
        
        # Vertex map
        self.v2dof = vertex_to_dof_map(adjoint.psi_int.function_space())
        self.dof2v = dof_to_vertex_map(adjoint.psi_int.function_space())
        self.indices = adjoint.node_markers.array() != 0
        self.index_map = self.v2dof[self.indices]
        
        # Get initial psi_int
        self.get_psi_int(adjoint, adjoint.psi0)
        
    def solve(self, adjoint):
        # assemble matrices
        A = assemble(self.a)
        b = assemble(self.L)
        
        # apply boundary conditions
        [bc.apply(A, b) for bc in self.bc]
        
        #solve for psi
        self.solver.solve(A, adjoint.psi.vector(), b)
        adjoint.psi0.assign(adjoint.psi)
        self.get_psi_int(adjoint, adjoint.psi0)
        
    def get_psi_int(self, adjoint, psi):
        for idx in self.index_map:
            vert = self.dof2v[idx]
            x = Vertex(adjoint.mesh, vert).point()[0]
            y = sqrt(1 + adjoint.h_x(x) ** 2) * psi(x)
            adjoint.psi_int.vector()[idx] = y        