# -*- coding: utf-8 -*-
"""
Solver for adjoint heat eqaution

Created on Mon Nov 30 11:31:00 2015

@author: baran
"""

from dolfin import *

class Solver():
    "Compute omega the adjoint temperature on the whole domain."
    "The mesh including its movement is known from the forward cpmputations."
    
    def __init__(self, adjoint):
        # Define Functions
        self.omega = TrialFunction(adjoint.CG2)
        self.v = TestFunction(adjoint.CG2)
        adjoint.omega0 = Function(adjoint.CG2) # initial condition
        
        # Define boundary conditions
        def BCzero(x, on_boundary):
            return adjoint.BCcool(x, on_boundary) or adjoint.BCheat(x, on_boundary)
        self.BCzero = BCzero
        class Outflow(SubDomain):
            def inside (self_, x, on_boundary):
                return adjoint.BCoutflow(x, on_boundary)
        self.Outflow = Outflow
        self.outflow = Outflow()
        self.outflow_facet_markers = FacetFunction("size_t", adjoint.mesh)
        self.outflow_facet_markers.set_all(0)
        self.outflow.mark(self.outflow_facet_markers, 1)
        
        
        # Measure for integral over inflow boundary
        self.dS_out = Measure("ds", subdomain_data=self.outflow_facet_markers)
        self.my = FacetNormal(adjoint.mesh)
        
        # Bilinear and linear forms
        self.a1 = self.omega * self.v / adjoint.dt * dx
        self.a2 = inner(adjoint.u - adjoint.V_all, grad(self.omega)) * self.v * dx
        self.a3 = adjoint.alpha * inner(grad(self.omega), grad(self.v)) * dx
        self.a4 = inner(adjoint.u, self.omega * self.my) * self.v * self.dS_out(1)
        self.a = self.a1 + self.a2 + self.a3 + self.a4
        self.L = adjoint.omega0 * self.v / adjoint.dt * dx
        
        # initialize omega
        adjoint.omega = Function(adjoint.CG2)
        
        # Solver
        self.solver = LinearSolver()
        
    def solve(self, adjoint):
        # Update boundary conditions
        self.outflow_facet_markers.set_all(0)
        self.outflow.mark(self.outflow_facet_markers, 1)
        self.bc_zero = DirichletBC(adjoint.CG2, 0, self.BCzero)
        self.bc_interface = DirichletBC(adjoint.CG2, adjoint.psi_int, adjoint.facet_markers, 1)
        self.bcs = [self.bc_interface, self.bc_zero]
        
        # assemble matrices
        A = assemble(self.a)
        b = assemble(self.L)
        
        # apply boundary conditions
        [bc.apply(A, b) for bc in self.bcs]
        
        # solve for omega
        self.solver.solve(A, adjoint.omega.vector(), b)
        adjoint.omega0.assign(adjoint.omega)