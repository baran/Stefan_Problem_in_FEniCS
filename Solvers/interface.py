__author__ = "Bjoern Baran <baran@mpi-magdeburg.mpg.de>"
__date__ = "2015-07-22"
__copyright__ = "Copyright (C) 2015 " + __author__
__license__  = ""

from dolfin import *
from numpy import zeros, empty
from helpers import move_mesh, interface_normal, values_on_interface_to_vector_function
from scipy.linalg import solve as solve_la

class Solver():
    "Solve Stefan condition for the interface velocity"
    
    def __init__(self, problem):
        
        # Measure for integral over interface
        self.dS_int = Measure('dS', subdomain_data=problem.facet_markers)      
        
        # Create the bililear and linear forms
        self.a = inner(avg(problem.V_var), avg(problem.r)) * self.dS_int(1)
        self.L = (problem.ks * inner(problem.normals, grad(problem.T)('+')) - \
        problem.kl * inner(problem.normals, grad(problem.T)('-'))) \
        / problem.La * avg(problem.r) * problem.dt * self.dS_int(1)

        # dof map
        self.vtx2dof = vertex_to_dof_map(problem.Q)
        self.indices = problem.node_markers.array() != 0
        self.index_map = self.vtx2dof[self.indices]
        
    def solve(self, problem):
        "solve one timestep"
        
        # Interface normal
        n_ = interface_normal(problem.mesh, problem.node_markers, \
        problem.facet_markers, problem.T)
        # Create interface normal vectors embeded on whole domain
        n = empty(problem.node_markers.array().shape, dtype=Point)
        n[self.indices] = n_
        normals_ = zeros(problem.node_markers.array().shape)
        normals_[self.indices] = 1.;
        values_on_interface_to_vector_function(problem.normals, normals_, n, \
        problem.mesh, problem.node_markers)
        # assemble matrices for interface velocity
        b_var = assemble(self.L)
        A_var = assemble(self.a)
        # reduce to interface dofs
        A_interface = A_var.array()[self.index_map, :]
        A_interface = A_interface[ : , self.index_map]
        b_interface = b_var.array()[self.index_map]
        # solve for interface velocity
        V_interface_ = solve_la(A_interface, b_interface)
        # Velocitys of interface and rest zero
        V_interface = zeros(problem.node_markers.array().shape)
        V_interface[self.indices] = V_interface_
        
        # Create verctor field for moving the interface nodes
        move_mesh(problem.V_int, V_interface, n, problem.mesh, problem.node_markers)