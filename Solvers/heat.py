__author__ = "Bjoern Baran <baran@mpi-magdeburg.mpg.de>"
__date__ = "2015-07-22"
__copyright__ = "Copyright (C) 2015 " + __author__
__license__  = ""

from dolfin import *
from numpy import zeros

class Solver():
    "Solve heat equation on solid and liquid phase"
    
    def __init__(self, problem):
        
        # Function spaces
        self.CG2 = problem.CG2
        
        # Trial and test functions
        self.T = problem.T # temperature
        self.w = problem.w # testfunction
        
        
        # Parameter values
        self.dt = problem.k # time step
        self.ks = problem.ks # thermal conductivity of solid
        self.kl = problem.kl # thermal conductivity of liquid
        self.alpha = problem.alpha
        self.T_heat = problem.T_heat # temperature of the warming canal
        self.T_cool = problem.T_cool # temperature of the freezing canal
        
        # Boundary conditions
        self.BCheat = problem.BCheat # warming canal
        self.BCcool = problem.BCcool # cooling canal
        self.bc_interface = problem.bc_interface
        
        # Create the bililear and linear forms
        self.a = problem.T * problem.w / problem.dt * dx + \
        inner((problem.u0 - problem.V_all), grad(problem.T)) * \
        problem.w * dx +  \
            inner(problem.alpha * grad(problem.T), grad(problem.w)) * dx 
        self.L = problem.T0 * problem.w / problem.dt * dx
        
        # initialize temperature
        problem.T = problem.T0        
        
        # Solver
        self.solver = LinearSolver()
        
        # matrices to store solutions for every time step
        n_dof = problem.T.vector().size()
        problem.T_mat = zeros((n_dof, problem.t.size))
        problem.T_mat[ : , 0] = problem.T.vector().copy().array()
        
    def solve(self, problem):
        "solve one timestep"
        
        # assemble matrices
        A = assemble(self.a)
        b = assemble(self.L)
        # update boundary position
        bc_heat = DirichletBC(problem.CG2, problem.T_heat, problem.BCheat)
        bc_cool = DirichletBC(problem.CG2, problem.T_cool, problem.BCcool)
        bcs_temp = [bc_heat, bc_cool, problem.bc_interface]
        # apply boundary conditions
        [bc.apply(A, b) for bc in bcs_temp]
        # solve for temperature
        self.solver.solve(A, problem.T.vector(), b)
        problem.T_mat[ : , problem.i] = problem.T.vector().copy().array()
        