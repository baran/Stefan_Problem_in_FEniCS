__author__ = "Bjoern Baran <baran@mpi-magdeburg.mpg.de>"
__date__ = "2015-07-22"
__copyright__ = "Copyright (C) 2015 " + __author__
__license__  = ""

# List of available solvers
solvers = ["Chonin", "mesh_movement", "heat", "interface", "Newton_NSE", "graph"]

# Wrapper for different solvers
def Solver(name, problem):
    "Return the NSE solver for the given solver name"
    exec("from Solvers.%s import Solver as requested_solver" % name, globals())
    return requested_solver(problem)