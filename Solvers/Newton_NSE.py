__author__ = "Bjoern Baran <baran@mpi-magdeburg.mpg.de>"
__date__ = "2015-07-22"
__copyright__ = "Copyright (C) 2015 " + __author__
__license__  = ""

from dolfin import *
from numpy import zeros, logspace, concatenate, array
from helpers import map_function_from_submesh_to_mesh

class Solver():
    "Solve NSE with Newtons method and mixed function space"
    
    def __init__(self, problem):
        
        # Create mixed function space and functions
        class Liquid(SubDomain):
            def inside(self_, x, on_boundary):
                return problem.T0(x) > 0 - problem.delta
        problem.liquid = Liquid()
        problem.mesh_l = SubMesh(problem.mesh, problem.liquid)
#        self.V = VectorFunctionSpace(problem.mesh_l, "Lagrange", 2) # velocity
        P2 = VectorElement("Lagrange", problem.mesh_l.ufl_cell(), 2) # velocity
        P1 = FiniteElement("Lagrange", problem.mesh_l.ufl_cell(), 1) # preasure
        TH = P2 * P1
        self.M = FunctionSpace(problem.mesh_l, TH)
        problem.W_l = VectorFunctionSpace(problem.mesh_l, "Lagrange", 1) # velocity to project back
        problem.CG1_l = FunctionSpace(problem.mesh_l, "Lagrange", 1) # preasure
#        self.M = self.V * problem.CG1_l
        self.w = Function(self.M)
        w = TrialFunction(self.M)
        (self.u, self.p) = split(w)
        (self.v, self.q) = TestFunctions(self.M)
        self.V_all = Function(problem.W_l)
        self.u0 = Function(problem.W_l)
        self.p_in = interpolate(Constant(problem.p_in[0]), problem.CG1_l)
        # Mark facets on inflow boundary
        class Inflow(SubDomain):
            def inside(self_, x, on_boundary):
                return problem.BCheat(x, on_boundary)
        self.Inflow = Inflow
        self.inflow = Inflow()
        self.inflow_facet_markers = FacetFunction("size_t", problem.mesh_l)
        self.inflow_facet_markers.set_all(0)
        self.inflow.mark(self.inflow_facet_markers, 1)
        
        # Measure for integral over inflow boundary
        self.dS_in = Measure("ds", subdomain_data=self.inflow_facet_markers)
        
        # Redefine boundary conditions for mixed function space
        self.noslip_interface  = DirichletBC(self.M.sub(0), (0, 0),problem.BCslip)
        self.noslip= DirichletBC(self.M.sub(0), (0, 0), problem.BCrest)
        self.bc_velocity = [self.noslip, self.noslip_interface]
        
        # outer unit normal
        self.my = FacetNormal(problem.mesh_l)
        
        # to get a good "initial" guess solve with smaller time step size first
        self.eta_range = logspace(1, 1, problem.eta_count + 1, base=problem.eta)
        self.dt_range = array([problem.dt])
        self.V0 = FunctionSpace(problem.mesh_l, 'DG', 0)
        self.eta = interpolate(Constant(self.eta_range[0]), self.V0)
        self.dt = interpolate(Constant(problem.dt), self.V0)
        
        # Create the bililear and linear forms
        self.a1 = (1. / self.dt) * inner(self.u , self.v) * dx
        self.a2 = inner(grad(self.u) * (self.u - self.V_all), self.v) * dx
        self.a3 = self.eta * inner(grad(self.u), grad(self.v)) * dx
        self.a4 = -1. * div(self.v) * self.p * dx
        self.a5 = -1. * div(self.u) * self.q * dx
        self.a = self.a1 + self.a2 + self.a3 + self.a4 + self.a5
        self.L1 = (1. / self.dt) * inner(self.u0, self.v) * dx
        self.L2 = -1. * inner(self.p_in * self.my, self.v) * self.dS_in(1)
        self.L = self.L1 + self.L2
        self.F = self.a - self.L
        self.w_ = Function(self.M)
        self.w_old = Function(self.M)
        (self.u1, self.p1) = self.w_.split()
        (self.u1_, self.p1_) = self.w_.split(deepcopy=True)
        self.F = action(self.F, self.w_)
        
        # project back 
        map_function_from_submesh_to_mesh(problem.mesh_l, problem.W_l, self.u1_, \
            problem.mesh, problem.W, problem.u1_)
        
        # Functions
        self.zero = Function(problem.W_l)
        self.zero_ = Function(problem.W)
        
        # matrices to store solutions for every time step
        n_dof = problem.u1_.vector().size()
        problem.u_mat = zeros((n_dof, problem.t.size))
        problem.u_mat[ : , 0] = problem.u1_.vector().copy().array()
        
    def solve(self, problem):
        "solve one timestep"
        # check weather velocity is too small to give resonable values due to f
        # frozen outflow
        if  (problem.i > 1) and (problem.u1_.vector().array().max() < 1e-5):
            self.u1.assign(self.zero)
            self.p1.assign(self.zero)
            problem.u1_.assign(self.zero_)
            problem.p1_.assign(self.zero_)
        else:
            # Project V_all onto submesh
            self.V_all.assign(problem.V_all_l)
            # move submesh
            ALE.move(problem.mesh_l, SubMesh(problem.mesh, problem.liquid))
            problem.mesh_l.bounding_box_tree().build(problem.mesh_l)
            # Project down u0
            u0 = interpolate(problem.u0, problem.W_l)
            self.u0.assign(u0)
            # reset boundaries
            self.inflow_facet_markers.set_all(0)
            self.inflow.mark(self.inflow_facet_markers, 1)
            p_in = interpolate(Constant(problem.p_in[problem.i]), problem.CG1_l)
            self.p_in.assign(p_in)
            
            # Measure for integral over inflow boundary
            self.dS_in = Measure("ds", subdomain_data=self.inflow_facet_markers)
            
            # Redefine boundary conditions for mixed function space
            self.noslip_interface  = DirichletBC(self.M.sub(0), (0, 0),problem.BCslip)
            self.noslip= DirichletBC(self.M.sub(0), (0, 0), problem.BCrest)
            self.bc_velocity = [self.noslip, self.noslip_interface]
            # assemble system
            # Build derivative of F
            dw =  TrialFunction(self.M)
            dF = derivative(self.F, self.w_, dw)
            
            self.NSE_problem = NonlinearVariationalProblem(self.F, self.w_, \
                self.bc_velocity, dF)
            
            # Solver
            self.solver = NonlinearVariationalSolver(self.NSE_problem)
            prm = self.solver.parameters
            prm["newton_solver"]["maximum_iterations"] = 15
            
            i = 0
            while i < self.dt_range.shape[0]:
                # Set (smaller) time step size
                self.dt.vector()[:] = self.dt_range[i]
                self.w_old.assign(self.w_)
                
                # solve system
                try:
                    self.solver.solve()
                    (u0, p0) = self.w_.split(deepcopy=True)
                    self.u0.assign(u0)
                    i = i + 1
                except Exception:
                    self.w_.assign(self.w_old)
                    if i == 0:
                        self.dt_range = array([self.dt_range[i] / 2., self.dt_range[i] / 2.])
                        print("Time step size %1.5f" % (self.dt_range[i]))
                    else:
                        mid = array([self.dt_range[i] / 2., self.dt_range[i] / 2.])
                        self.dt_range = concatenate((self.dt_range[0 : i], mid, self.dt_range[i + 1 : ]))
                        print("Time step size %1.5f" % (self.dt_range[i]))
                    
            # reset time step
            self.dt_range = array([problem.dt])
            self.dt.vector()[:] = self.dt_range[0]
            # for unsing in heat equation copy does not work ?
            (self.u1, self.p1) = self.w_.split()
            # for storing the time step solutions
            (self.u1_, problem.p1_) = self.w_.split(deepcopy=True)
            self.u1_ = interpolate(self.u1_, problem.W_l)
            
            # project back 
            map_function_from_submesh_to_mesh(problem.mesh_l, problem.W_l, \
                self.u1_, problem.mesh, problem.W, problem.u1_)
            
            problem.u_mat[ : , problem.i] = problem.u1_.vector().copy().array()