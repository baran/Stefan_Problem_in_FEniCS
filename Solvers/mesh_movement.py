__author__ = "Bjoern Baran <baran@mpi-magdeburg.mpg.de>"
__date__ = "2015-07-22"
__copyright__ = "Copyright (C) 2015 " + __author__
__license__  = ""

from dolfin import *
from helpers import check_boundary_velocitys
from numpy import zeros

class Solver():
    "Solve Laplace term to extend interface movement"
    
    def __init__(self, problem):
        
        # Create the bililear and linear forms
        self.a = inner(grad(problem.V_all), grad(problem.v_v)) * dx
        self.L = inner(problem.zero , problem.v_v) * dx
        
        # Solver
        self.solver_V = LinearSolver()
        
        # Initialize Function
        problem.V_all = Function(problem.W)
        # project down on submesh of liquid for NSE
        problem.V_all_l = interpolate(problem.V_all, problem.W_l)
        
        # matrices to store solutions for every time step
        n_dof = problem.V_all.vector().size()
        problem.V_all_mat = zeros((n_dof, problem.t.size))
        problem.V_all_mat[ : , 0] = problem.V_all.vector().copy().array()
        n_dof = problem.V_all_l.vector().size()
        problem.V_all_l_mat = zeros((n_dof, problem.t.size))
        problem.V_all_l_mat[ : , 0] = problem.V_all_l.vector().copy().array()
        
    def solve(self, problem):
        "solve one timestep"
        
        # assemble matrices
        A = assemble(self.a)
        b = assemble(self.L)
        # apply bounday conditions
        problem.bc_outside_expand_region.apply(A, b)
        problem.bc_interface_v.apply(A, b)
        # compute expansion
        self.solver_V.solve(A, problem.V_all.vector(), b)
        # make sure the shape of the domain is not changed
        problem.V_all = check_boundary_velocitys(problem.V_all, \
        problem.nodes_on_boundary)
        problem.V_all_mat[ : , problem.i] = problem.V_all.vector().copy().array()
        # project down on submesh of liquid for NSE
        problem.V_all_l = interpolate(problem.V_all, problem.W_l)
        problem.V_all_l_mat[ : , problem.i] = problem.V_all_l.vector().copy().array()