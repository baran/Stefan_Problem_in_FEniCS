__author__ = "Bjoern Baran <baran@mpi-magdeburg.mpg.de>"
__date__ = "2015-11-23"
__copyright__ = "Copyright (C) 2015 " + __author__
__license__  = ""

from dolfin import *
from scipy import zeros
from math import ceil

class Solver():
    "Compute graph values which determine interface position over lower bounary."
    "Build a function with these"
    
    def __init__(self, problem):
        
        # Build submesh of lower boundary
        mesh_size = 70
        problem.boundary_mesh = UnitIntervalMesh(mesh_size)
        self.fine_mesh = UnitIntervalMesh(mesh_size * 2)

        
        # Define function for graph
        problem.H = FunctionSpace(problem.boundary_mesh, 'CG', 2)
        problem.CG1_boundary = FunctionSpace(problem.boundary_mesh, 'CG', 1)
        self.H = FunctionSpace(self.fine_mesh, 'CG', 1)
        problem.h = Function(problem.H)
        self.h = Function(self.H)
        problem.h_x = project(Dx(problem.h, 0), problem.CG1_boundary)
        
        # dof map
        self.vertex_to_dof = vertex_to_dof_map(self.H)
        
        # Set graph values
        self.get_h(problem)
        
        # initize desired graph
        problem.h_d = Function(problem.H)
        
        # matrices to store solutions for every time step
        n_dof = problem.h.vector().size()
        problem.h_mat = zeros((n_dof, problem.t.size))
        problem.h_mat[ : , 0] = problem.h.vector().copy().array()
        n_dof_x = problem.h_x.vector().size()
        problem.h_x_mat = zeros((n_dof_x, problem.t.size))
        problem.h_x_mat[ : , 0] = problem.h_x.vector().copy().array()
        
    def update(self, problem):
        "update h"
        
        # Set graph values
        self.get_h(problem)
        problem.h_x = project(Dx(problem.h, 0), problem.CG1_boundary)
        problem.h_mat[ : , problem.i] = problem.h.vector().copy().array()
        problem.h_x_mat[ : , problem.i] = problem.h_x.vector().copy().array()

    def get_h(self, problem):
        "fill h with actual interface position"
        int_c = problem.mesh.coordinates()[problem.node_markers.array() != 0]
        int_c = int_c[int_c[ : , 0].argsort()]
        self.get_graph(self.h, int_c)
        problem.h = interpolate(self.h, problem.H)

    def get_graph(self, h, array):
        X = h.function_space().tabulate_dof_coordinates()
        for i, x in enumerate(X):
            pos_c = self.bi_serach(x, array)
            if array[pos_c, 0] == x:
                y = array[pos_c, 1]
            else:
                y = array[pos_c, 1] + (array[pos_c + 1, 1] - array[pos_c, 1]) \
                    / (array[pos_c+ 1, 0] - array[pos_c, 0]) * (x - array[pos_c, 0])
            h.vector()[self.vertex_to_dof[-i - 1]] = y

    def get_h_x(self, problem):
        "fill h with actual interface position"
        int_c = problem.mesh.coordinates()[problem.node_markers.array() != 0]
        int_c = int_c[int_c[ : , 0].argsort()]
        X = problem.H.dofmap().tabulate_all_coordinates(problem.boundary_mesh)
        n = int_c.shape[0]
        for i, x in enumerate(X):
            pos_c = self.bi_serach(x, int_c)
            if int_c[pos_c, 0] == x:
                if pos_c > 0:
                    y1 = (int_c[pos_c, 1] - int_c[pos_c - 1, 1]) \
                        / (int_c[pos_c, 0] - int_c[pos_c - 1, 0])
                else:
                    y1 = 0
                if pos_c < n - 1:
                    y2 = (int_c[pos_c + 1, 1] - int_c[pos_c, 1]) \
                        / (int_c[pos_c + 1, 0] - int_c[pos_c, 0])
                else:
                    y2 = 0
                if x == 0:
                    y = y2
                elif x == 1:
                    y = y1
                else:
                    y = (y1 + y2) / 2.
            else:
                y = (int_c[pos_c + 1, 1] - int_c[pos_c, 1]) \
                    / (int_c[pos_c+ 1, 0] - int_c[pos_c, 0])
            problem.h_x.vector()[self.vertex_to_dof[-i - 1]] = y
            
    def bi_serach(self, x, array):
        "search for position of x in array"
        a = 0
        b = array.shape[0]
        while a < b:
            m = ceil((a + b) / 2.)
            if array[m, 0] == x:
                return m
            elif array[m, 0] < x:
                a = m
            else:
                b = m - 1
        return a