# -*- coding: utf-8 -*-
"""
Solver for adjoint NSE on submesh of the liquid part of the domain

Created on Mon Nov 30 14:38:44 2015

@author: baran
"""

from dolfin import *

class Solver():
    "Compute gamma the adjoint velocity and pi the adjoint preasure on the liquid"
    "part of the domain. The mesh including its movement is known from the"
    "forward cpmputations."
    
    def __init__(self, adjoint):
        # Define Functions 
        self.w = TrialFunction(adjoint.M)
        (self.gamma, self.pi) = split(self.w)
        (self.v, self.q) = TestFunctions(adjoint.M)
        adjoint.w0 = Function(adjoint.M)
        (adjoint.gamma0, adjoint.pi0) = adjoint.w0.split(deepcopy=True)
        self.omega = Function(adjoint.CG2_l)
        self.T = Function(adjoint.CG2_l)
        self.omega.assign(interpolate(adjoint.omega, adjoint.CG2_l))
        self.T.assign(interpolate(adjoint.T, adjoint.CG2_l))
        self.u = Function(adjoint.VCG1_l)
        self.u.assign(interpolate(adjoint.u, adjoint.VCG1_l))
        
        # Define boundary conditions
        def BCzero(x, on_boundary):
            return adjoint.BCslip(x, on_boundary) or adjoint.BCrest(x, on_boundary)
        self.BCzero = BCzero
        class InOut(SubDomain):
            def inside(self_, x, on_boundary):
                return adjoint.BCheat(x, on_boundary) or adjoint.BCoutflow(x, on_boundary)
        self.inout = InOut()
        self.flow_marker = FacetFunction("size_t", adjoint.mesh_l)
        self.flow_marker.set_all(0)
        self.inout.mark(self.flow_marker, 1)
        
        
        # Measure for integral over flow boundaries
        self.dS_inout = Measure("ds", subdomain_data=self.flow_marker)
        self.my = FacetNormal(adjoint.mesh_l)
        
        # Bilinear and linear forms
        self.a1 = inner(self.gamma, self.v) / adjoint.dt * dx
        self.a2 = inner(grad(self.u).T * self.gamma, self.v) * dx
        self.a3 = -1. * inner(grad(self.gamma) * (self.u - adjoint.V_all_l), self.v) * dx
        self.a4 = div(self.v) * self.pi * dx
        self.a5 = adjoint.eta * inner(grad(self.gamma), grad(self.v)) * dx
        self.a6 = inner(self.gamma, self.my) * inner(self.u - adjoint.V_all_l, self.v) * self.dS_inout(1)
        self.a7 = -1. * div(self.gamma) * self.q * dx
        self.a = self.a1 + self.a2 + self.a3 + self.a4 + self.a5 + self.a6 + self.a7
        self.L1 = inner(adjoint.gamma0, self.v) / adjoint.dt * dx
        self.L2 = -1. * self.omega * inner(grad(self.T), self.v) * dx
        self.L = self.L1 + self.L2
        
        # Initialize function
        adjoint.w = Function(adjoint.M)
        (adjoint.gamma, adjoint.pi) = split(adjoint.w)
        
        # Solver
        self.solver = LinearSolver()
        
    def solve(self, adjoint):
        # update omega and T
        self.omega.assign(interpolate(adjoint.omega, adjoint.CG2_l))
        self.T.assign(interpolate(adjoint.T, adjoint.CG2_l))
        self.u.assign(interpolate(adjoint.u, adjoint.VCG1_l))
        
        # update boundary conditions
        self.bc_zero = DirichletBC(adjoint.M.sub(0), (0, 0), self.BCzero)
        self.bcs = [self.bc_zero]
        self.flow_marker.set_all(0)
        self.inout.mark(self.flow_marker, 1)
        
        # assemble matrices
        A = assemble(self.a)
        b = assemble(self.L)
        
        # apply boundary conditions
        [bc.apply(A, b) for bc in self.bcs]
        
        # solve for omega
        self.solver.solve(A, adjoint.w.vector(), b)
        (adjoint.gamma, adjoint.pi) = split(adjoint.w)
        (gamma_, pi_) = adjoint.w.split(deepcopy=True)
        adjoint.w0.assign(adjoint.w)
        adjoint.gamma0.assign(gamma_)
        adjoint.pi0.assign(pi_)
        