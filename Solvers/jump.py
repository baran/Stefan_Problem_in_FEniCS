__author__ = "Bjoern Baran <baran@mpi-magdeburg.mpg.de>"
__date__ = "2015-11-25"
__copyright__ = "Copyright (C) 2015 " + __author__
__license__  = ""

from dolfin import *
from scipy import zeros
from scipy.linalg import solve as solve_la

class Solver():
    "Solve Jump condition in y direction and map to lower boundary"
    
    def __init__(self, problem):
        
        # Functionspace and functions
        problem.CG1 = FunctionSpace(problem.boundary_mesh, 'CG', 1)
        v = TestFunction(problem.Q)
        j = TrialFunction(problem.Q)
        self.y = interpolate(Constant((0, 1)), problem.W)
        self.j_interface = Function(problem.Q)
        problem.jump_y = Function(problem.CG1)
        self.j2_interface = Function(problem.Q)
        problem.jump_y2 = Function(problem.CG1)
        
        # Measure for integral over interface
        self.dS_int = Measure('dS', subdomain_data=problem.facet_markers)  
        
        # Create the bililear and linear forms
        self.a = inner(avg(j), avg(v)) * self.dS_int(1)
        self.L = (problem.ks * problem.T.dx(1)('+') - \
        problem.kl * problem.T.dx(1)('-')) \
         * avg(v) * self.dS_int(1) 
        self.L2 = (problem.ks * problem.T.dx(1).dx(1)('+') - \
        problem.kl * problem.T.dx(1).dx(1)('-')) \
         * avg(v) * self.dS_int(1) 

        # dof map
        self.vtx2dof_global = vertex_to_dof_map(problem.Q)
        self.vtx2dof_boundary = vertex_to_dof_map(problem.CG1)
        # indices of vertices on interface
        self.indices = self.vtx2dof_global[problem.node_markers.array() != 0]
        
        # matrices to store solutions for every time step
        n_dof = problem.jump_y.vector().size()
        problem.jump_y_mat = zeros((n_dof, problem.t.size))
        n_dof = problem.jump_y2.vector().size()
        problem.jump_y2_mat = zeros((n_dof, problem.t.size))
        self.solve(problem)
        
    def solve(self, problem):
        "solve one timestep"
        b_var = assemble(self.L)
        b2_var = assemble(self.L2)
        A_var = assemble(self.a)
        # reduce to interface dofs
        
        A_interface = A_var.array()[self.indices, :]
        A_interface = A_interface[ : , self.indices]
        b_interface = b_var.array()[self.indices]
        b2_interface = b2_var.array()[self.indices]
        # solve for interface velocity
        J_interface_ = solve_la(A_interface, b_interface)
        J2_interface_ = solve_la(A_interface, b2_interface)
        # Velocitys of interface and rest zero
        self.j_interface.vector()[self.indices] = J_interface_
        self.get_jump(self.j_interface, problem.jump_y, problem.h)
        self.j2_interface.vector()[self.indices] = J2_interface_
        self.get_jump(self.j2_interface, problem.jump_y2, problem.h)
        
        # store solution
        problem.jump_y_mat[ : , problem.i] = problem.jump_y.vector().copy().array()
        problem.jump_y2_mat[ : , problem.i] = problem.jump_y2.vector().copy().array()

    def get_jump(self, j_int, jump, h):
        X = jump.function_space().tabulate_dof_coordinates()
        for i, x in enumerate(X):
            y = j_int(x, h(x)) # if this doesn't hit the interface exactly this can be very inexact
            jump.vector()[self.vtx2dof_boundary[-i - 1]] = y