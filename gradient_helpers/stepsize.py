# -*- coding: utf-8 -*-
"""
Compute the stepsize for the gradient method

Created on Wed Dec  2 13:03:29 2015

@author: baran
"""

from scipy.optimize import minimize
from scipy.interpolate import interp1d
from scipy import linspace, minimum, maximum

class Stepsize():
    def __init__(self):
        self.tol = 1e-4
        self.max_it = 5
        self.eps = 1e-12 
        self.eps_min = 2e-8
        self.gain_tol = 1e-4
        self.grow_tol = 1e-1
        self.gain_grow_mult = 5
        self.last_try_step = 0.05
        
    def get_s(self, forward, update, control):
        s0 = 0
        s1 = 2
        s2 = 4
        i = 0
        gain = 1. 
        last_try = 0
        k_0 = forward.cost_value
        cont_1 = self.project(control + s1 * update, forward.problem)
        print("test stepsize %g" % s1)
        forward.reset(cont_1)
        forward.solve()
        k_1 = forward.cost_value
        cont_2 = self.project(control + s2 * update, forward.problem)
        print("test stepsize %g" % s2)
        forward.reset(cont_2)
        forward.solve()
        k_2 = forward.cost_value
        last_computed = 2
        while (i < self.max_it) and (gain > self.gain_tol * k_1):
            if False and (i == 0) and (abs(k_1 - k_2) < self.eps_min) and (not last_try):
                s_new = 0.
            else:
                f = interp1d([s0, s1, s2], [k_0, k_1, k_2], 'quadratic')
                x = linspace(s0, s2, 100000)
                y = f(x)
                s_new0 = x[y.argmin()]
                res = minimize(f, s1, method='L-BFGS-B', \
                bounds=[[s0 + self.eps_min, s2 - self.eps_min]], options={'gtol': 1e-10, 'ftol': 1e-10})
                s_new1 = res.x[0]
                if f(s_new0) < f(s_new1):
                    s_new = s_new0
                else:
                    s_new = s_new1
            if abs(s_new - s2) < self.eps:
                s0 = s1
                s1 = s2
                if abs(update).max() > self.grow_tol:
                    s2 = s2 + s1 - s0
                else:
                    s2 = 2 * s2
                k_0 = k_1
                k_1 = k_2
                cont_2 = self.project(control + s2 * update, forward.problem)
                print("test stepsize %g" % s2)
                forward.reset(cont_2)
                forward.solve()
                k_2 = forward.cost_value
                gain = abs(k_2 - k_1) * self.gain_grow_mult
                last_computed = 2
            elif (s_new < self.eps) and (not last_try):
                s1 = self.last_try_step
                s2 = 2 * s1
                cont_1 = self.project(control + s1 * update, forward.problem)
                print("test stepsize %g (last try mode)" % s1)
                forward.reset(cont_1)
                forward.solve()
                k_1 = forward.cost_value
                cont_2 = self.project(control + s2 * update, forward.problem)
                print("test stepsize %g (last try mode)" % s2)
                forward.reset(cont_2)
                forward.solve()
                k_2 = forward.cost_value
                gain = 1.
                last_computed = 2
                i = i + 1
                last_try = 1
            elif abs(s_new - s2) / s2 < self.tol:
                return s2, (last_computed != 2)
            elif abs(s_new - s1) / s1 < self.tol:
                return s1, (last_computed != 1)
            elif abs(s_new - s0) / s1 < self.tol:
                return s0, (last_computed != 0)
            elif s_new > s1:
                s0 = s1
                s1 = s_new
                k_0 = k_1
                cont_1 = self.project(control + s1 * update, forward.problem)
                print("test stepsize %g" % s1)
                forward.reset(cont_1)
                forward.solve()
                k_1 = forward.cost_value
                gain = min(abs(k_1 - k_0), abs(k_1 - k_2))
                last_computed = 1
                i = i + 1
            else:
                s2 = s1
                s1 = s_new
                k_2 = k_1
                cont_1 = self.project(control + s1 * update, forward.problem)
                print("test stepsize %g" % s1)
                forward.reset(cont_1)
                forward.solve()
                k_1 = forward.cost_value
                gain = min(abs(k_1 - k_2), abs(k_1 - k_0))
                last_computed = 1
                i = i + 1
        if k_0 < k_1:
            if k_0 < k_2:
                return s0, last_computed != 0
            else:
                return s2, last_computed != 2
        elif k_2 < k_1:
            return s2, last_computed != 2
        else:
            return s1, last_computed != 1
            
    def project(self, control, problem):
        "project control to admissible control"
        p_min = problem.p_min
        p_max = problem.p_max
        return maximum(minimum(control, p_max), p_min)