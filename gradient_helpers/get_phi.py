# -*- coding: utf-8 -*-
"""
Compute phi times outer unit normal

Created on Tue Dec  1 12:42:07 2015

@author: baran
"""

from dolfin import *

class Phi():
    def __init__(self, adjoint):
        # mark inflow boundary
        class Inflow(SubDomain):
            def inside(self_, x, on_boundary):
                return (x[0] < DOLFIN_EPS) and (x[1] > .6) and (x[1] < .8)
        self.inflow = Inflow()
        self.facet_marker = FacetFunction("size_t", adjoint.mesh_l)
        self.facet_marker.set_all(0)
        self.inflow.mark(self.facet_marker, 1)
        self.node_marker = VertexFunction("size_t", adjoint.mesh_l)
        self.node_marker.set_all(0)
        self.inflow.mark(self.node_marker, 1)
        self.one = interpolate(Constant(1), adjoint.CG1_l)
        self.ds = Measure("ds", subdomain_data=self.facet_marker)
        # compute lenght of inflow boundary
        self.length = self.one * self.ds(1)
        # compute phi times outer unit normal
        self.n = FacetNormal(adjoint.mesh_l)
        self.phi = inner(-1. * adjoint.gamma, self.n) * self.ds(1)
        adjoint.phi = adjoint.problem.t * 0.
        
    def solve(self, adjoint):
        # update facets
        self.facet_marker.set_all(0)
        self.inflow.mark(self.facet_marker, 1)
        # compute lenght of inflow boundary
        leng = assemble(self.length)
        # compute phi times outer unit normal
        phi = assemble(self.phi)
        adjoint.phi[adjoint.i] = phi / leng