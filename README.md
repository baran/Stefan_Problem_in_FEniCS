## Stefan Problem in FEniCS
### This is the code developed for the Master thesis of Björn Baran (`master_thesis_Bjoern_Baran.pdf`):

### _Optimal Control of a Stefan Problem with Gradient-Based Methods in FEniCS_

The simulation and optimal control of a Stefan problem in FEniCS can be started

with 
```
python gradient_method.py
```
in Python.

The settings can be changed in `Problems/default_problem.py` and 

`Problems/settings.py`.

For examples see `Problems/settings_snapshots/`.

#### Dependencies:
* FEniCS 1.5.0
* SciPy 0.15.1
* NumPy 1.9.1
* Python 2.7.6

This project is licensed under the terms of the MIT license.

#### Test machines
The code was tested on the following three machines (with the same versions of FEniCS, SciPy, NumPy and Python):

# 1
* CPU type: Intel(R) Core(TM)2 Quad CPU Q6700 @ 2.66GHz
* RAM installed: 8 GB
* SWAP installed: 6 GB
* operating system: GNU/Linux
* kernel name: Linux
* kernel release: 3.2.0-97-generic
* kernel version: #137-Ubuntu SMP Thu Dec 17 18:11:47 UTC 2015
* plattfrom type: x86_64 (64Bit)
* Description:    Ubuntu 12.04.5 LTS
* Release:        12.04
* Codename:       precise
* Version of the GNU Compiler Collection: gcc\g++\gfortran (Ubuntu/Linaro 4.6.3-1ubuntu5) 4.6.3
* Version of the GNU C Library: libc: 2.15
* BLAS shared object version: Source: openblas, Version: 0.1alpha2.2-3
* LAPACK shared object version: Source: lapack, Version: 3.3.1-1

# 2
* CPU type: 2 x Intel(R) Core(TM)2 Quad CPU Q6700 @ 2.66GHz
* RAM installed: 32 GB
* SWAP installed: 34 GB
* operating system: GNU/Linux
* kernel name: Linux
* kernel release: 3.13.0-57-generic
* kernel version: #95-Ubuntu SMP Fri Jun 19 09:28:15 UTC 2015
* plattfrom type: x86_64 (64Bit)
* Description:    Ubuntu 14.04.2 LTS
* Release:        14.04
* Codename:       trusty
* Version of the GNU Compiler Collection: gcc\g++\gfortran (Ubuntu 4.8.4-2ubuntu1~14.04) 4.8.4
* Version of the GNU C Library: libc: libc: 2.19
* BLAS shared object version: Source: flexiblas, Version: 1.3.0
* LAPACK shared object version: Source: lapack, Version: 3.5.0-2ubuntu1

# 3
* CPU type: 4 x Intel(R) Xeon(R) CPU E7- 8837 @ 2.67GHz
* RAM installed: 1 TB
* SWAP installed: 6 GB
* operating system: GNU/Linux
* kernel name: Linux
* kernel release: 3.2.0-94-generic
* kernel version: #134-Ubuntu SMP Fri Nov 6 18:16:45 UTC 2015
* plattfrom type: x86_64 (64Bit)
* Description:    Ubuntu 12.04.5 LTS
* Release:        12.04
* Codename:       precise
* Version of the GNU Compiler Collection: gcc\g++\gfortran-4.6.real (Ubuntu/Linaro 4.6.3-1ubuntu5) 4.6.3
* Version of the GNU C Library: libc: libc: libc: 2.15
* BLAS shared object version: Source: openblas, Version: 0.2.14-1ubuntu1
* LAPACK shared object version: Source: openblas Version: 0.2.14-1ubuntu1