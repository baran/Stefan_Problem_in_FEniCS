# -*- coding: utf-8 -*-
"""
create mesh with python4gmsh
Created on Wed Jun 10 15:34:42 2015

@author: baran
"""
import python4gmsh as p4g
from numpy import arange

l_outer = 0.05
l_inner = 0.05
int_pos = 1. / 6.

p2 = p4g.Point([0., 0., 0.], l_outer)
p3 = p4g.Point([0., 1., 0.], l_outer)
p4 = p4g.Point([1., 1., 0.], l_outer)
p5 = p4g.Point([1., 0., 0.], l_outer)

no_inner_points = 30
points = []

for i in arange(no_inner_points + 1, dtype='double'):
    points.append(p4g.Point([i / no_inner_points, int_pos, 0.], l_inner))
    
lines = []
lines_outer = []

lines_outer.append(p4g.Line(p2, points[0]))
lines_outer.append(p4g.Line(points[no_inner_points], p4))
lines_outer.append(p4g.Line(p4, p3))
lines_outer.append(p4g.Line(p3, points[0]))
lines_outer.append(p4g.Line(points[no_inner_points], p5))
lines_outer.append(p4g.Line(p5, p2))

for i in arange(no_inner_points):
    lines.append(p4g.Line(points[i], points[i + 1]))

l = [lines_outer[0]]
for i in arange(no_inner_points):
    l.append(lines[i])

l.append(lines_outer[4])
l.append(lines_outer[5])
lineloop = p4g.LineLoop(l)
p4g.PlaneSurface(lineloop)

l = lines_outer[1 : 4]
for i in arange(no_inner_points):
    l.append(lines[i])
lineloop = p4g.LineLoop(l)
p4g.PlaneSurface(lineloop)

file = open('stefan_mesh.geo', 'w')
file.write(p4g.get_code())
file.close()