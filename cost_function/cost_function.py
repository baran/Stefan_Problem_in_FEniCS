# -*- coding: utf-8 -*-
"""
Cost function

Created on Tue Dec  1 11:37:25 2015

@author: baran
"""

from cost_function.control_cost import Control_Cost
from cost_function.interface_distance import distance
from scipy import zeros

class Cost_Function():
    def __init__(self, problem, graph):
        self.control = Control_Cost(problem)
        self.dist = distance()
        self.t = problem.t[1 : ] - problem.t[ : -1]
        self.d2 = zeros(self.t.shape[0])
        
    def eval(self, problem):
        p = self.control.cost
        d = self.dist.eval(problem)
        self.d2[problem.i - 1] = self.t[problem.i - 1] * d
        d2 = self.d2.sum()
        return p * problem.lambd + d * problem.Lambda + d2 * problem.Lambda2