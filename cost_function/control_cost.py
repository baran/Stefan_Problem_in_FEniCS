# -*- coding: utf-8 -*-
"""
Compute control cost

Created on Tue Dec  1 11:21:47 2015

@author: baran
"""
from dolfin import *

class Control_Cost():
    def __init__(self, problem):
        # mark inflow boundary
        class Inflow(SubDomain):
            def inside(self_, x, on_boundary):
                return (x[0] < DOLFIN_EPS) and (x[1] > .6) and (x[1] < .8)
        self.inflow = Inflow()
        self.facet_marker = FacetFunction("size_t", problem.mesh_l)
        self.facet_marker.set_all(0)
        self.inflow.mark(self.facet_marker, 1)
        self.ds = Measure("ds", subdomain_data=self.facet_marker)
        # compute lenght of inflow boundary
        self.one = interpolate(Constant(1), problem.CG1_l)
        self.length = self.one * self.ds(1)
        self.length = assemble(self.length)
        t = problem.t[1 : ] - problem.t[ : -1]
        self.cost = self.length * t.dot( abs(problem.p_in[1 : ]) ** 2)