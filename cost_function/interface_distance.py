__author__ = "Bjoern Baran <baran@mpi-magdeburg.mpg.de>"
__date__ = "2015-09-22"
__copyright__ = "Copyright (C) 2015 " + __author__
__license__  = ""

from dolfin import *
from scipy.integrate import quad
from math import ceil

class distance():
    "Distance function of the current interface position to te desired interface"
    "position."
        
    def eval(self, problem):
        a = ((problem.h - problem.h_d) ** 2) * dx
        return assemble(a)
        
    def eval_old(self, problem):
        "compute current distance"
        # coordinates of the actual interface position
        interface_current = problem.mesh.coordinates()[problem.node_markers.array() != 0]
        interface_current = interface_current[interface_current[ : , 0].argsort()]
        def integrand(x, int_c, int_d):
            # find x position in int_c
            pos_c = bi_serach(x, int_c)
            if int_c[pos_c, 0] == x:
                y_c = int_c[pos_c, 1]
            else:
                y_c = int_c[pos_c, 1] + (int_c[pos_c + 1, 1] - int_c[pos_c, 1]) \
                / (int_c[pos_c+ 1, 0] - int_c[pos_c, 0]) * (x - int_c[pos_c, 0])
            pos_d = bi_serach(x, int_d)
            if int_d[pos_d, 0] == x:
                y_d = int_d[pos_d, 1]
            else:
                y_d = int_d[pos_d, 1] + (int_d[pos_d + 1, 1] - int_d[pos_d, 1]) \
                / (int_d[pos_d+ 1, 0] - int_d[pos_d, 0]) * (x - int_d[pos_d, 0])
            return (y_c - y_d) ** 2
        I = quad(integrand, 0, 1, args=(interface_current, problem.interface_desired))
        return I[0]
            
            
def bi_serach(x, array):
    "search for position of x in array"
    a = 0
    b = array.shape[0]
    while a < b:
        m = ceil((a + b) / 2.)
        if array[m, 0] == x:
            return m
        elif array[m, 0] < x:
            a = m
        else:
            b = m - 1
    return a