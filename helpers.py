# -*- coding: utf-8 -*-
"""
Created on Thu May  7 13:37:21 2015

@author: baran
"""
from dolfin import *
from numpy import zeros, empty, arange, sign, array, dot, empty_like, where, concatenate 
from numpy.linalg import norm

def get_V(coordinates, T, ks, kl, L, n):
    V = zeros(coordinates.shape[0])
    for node_idx in range(coordinates.shape[0]):
        delta_x = norm(array([n[node_idx][0], n[node_idx][1]]))
        Td = T(coordinates[node_idx, : ])
        Ts = T(coordinates[node_idx, : ] - array([n[node_idx][0], n[node_idx][1]]))
        Tl = T(coordinates[node_idx, : ] + array([n[node_idx][0], n[node_idx][1]]))
        V[node_idx] = (kl * (Td - Tl) / delta_x - ks * (Ts - Td) / delta_x) / L
    return V

def values_on_interface_to_vector_function(function, values, vector, mesh, markers):
    V_new = zeros(function.vector().array().shape)
    vtx2dof = vertex_to_dof_map(function.function_space())
    vtx2dof.resize((int(vtx2dof.shape[0] / 2), 2))
    for node_idx in range(markers.array().shape[0]):
        if markers.array()[node_idx] != 0:
            V_new[vtx2dof[node_idx, : ]] = values[node_idx] \
            * array([vector[node_idx][0], vector[node_idx][1]]) \
            / norm(array([vector[node_idx][0], vector[node_idx][1]]))
    function.vector().set_local(V_new)
    

def move_mesh(V_vec, V_interface, n, mesh, markers):
    V_new = zeros(V_vec.vector().array().shape)
    vtx2dof = vertex_to_dof_map(V_vec.function_space())
    vtx2dof.resize((int(vtx2dof.shape[0] / 2), 2))
    for node_idx in range(markers.array().shape[0]):
        if (markers.array()[node_idx] == 1) or \
        ((markers.array()[node_idx] == 2) and (abs(n[node_idx][0]) < DOLFIN_EPS )):
            # inner node
            V_new[vtx2dof[node_idx, : ]] = V_interface[node_idx] \
            * array([n[node_idx][0], n[node_idx][1]]) \
            / norm(array([n[node_idx][0], n[node_idx][1]]))
        elif markers.array()[node_idx] == 2:
            # node is on boundary and should stay on boundary
            # get vertex
            ver = Vertex(mesh, node_idx)
            # get neighbor node in interface
            neighbor = get_neighbor(ver, mesh, markers)
            # compute next position of the neigbor after mesh movement
            neighbor_moved = mesh.coordinates()[neighbor, :] + \
            V_interface[neighbor] \
            * array([n[neighbor][0], n[neighbor][1]]) \
            / norm(array([n[neighbor][0], n[neighbor][1]]))
            # compute next position of node after mesh movement (not on domain anymore)
            node_moved = mesh.coordinates()[node_idx, :] + \
            V_interface[node_idx] \
            * array([n[node_idx][0], n[node_idx][1]]) \
            / norm(array([n[node_idx][0], n[node_idx][1]]))
            # compute intersection of new interface position with boundary
            new_node = seg_intersect(node_moved,neighbor_moved, \
            mesh.coordinates()[node_idx, :],\
            mesh.coordinates()[node_idx, :] + array([0, 1]))
            # compute movement vector
            V_new[vtx2dof[node_idx, : ]] = new_node - \
            mesh.coordinates()[node_idx, :]
    V_vec.vector().set_local(V_new)
    
def interface_normal(mesh, node_markers, facet_markers, T):
    delta_x = 0.2 * mesh.hmin()
    node_idx = arange(node_markers.array().shape[0])[node_markers.array() != 0]
    n = empty(node_idx.shape[0], dtype=Point)
    for i in arange(node_idx.shape[0]):
        ver = Vertex(mesh, node_idx[i])
        if node_markers.array()[node_idx[i]] == 2:
            n[i] = Point(0.0, 0.0)
            count = 0.0
            for fac in entities(ver, mesh.geometry().dim()-1):
                if facet_markers.array()[fac.index()] != 0:
                    f = Facet(mesh, fac.index())
                    normal = f.normal()
                    normal = normal * delta_x \
                    / norm(array([normal[0], normal[1]]))
                    # check whether normal points out of domain
                    if normal[1] < 0:
                        normal *= -1.
                    n[i] += normal
                    count+=1
            n[i] /= count
        else:
            n[i] = Point(0.0, 0.0)
            count = 0.0
            for fac in entities(ver, mesh.geometry().dim()-1):
                if facet_markers.array()[fac.index()] != 0:
                    f = Facet(mesh, fac.index())
                    normal = f.normal()
                    normal = normal * delta_x \
                    / norm(array([normal[0], normal[1]]))
                    # check whether normal points out of domain
                    new_point = mesh.coordinates()[node_idx[i], :] + \
                        array([normal[0], normal[1]])
                    # make sure the normal points into liquid phase
                    normal *= sign(T(new_point))
                    n[i] += normal
                    count+=1
            n[i] /= count
    return n

def interface_distance(mesh, facet_markers):
    '''http://fenicsproject.org/qa/1446/distance-to-boundary'''
    V = FunctionSpace(mesh, 'CG', 1)
    v = TestFunction(V)
    u = TrialFunction(V)
    f = Constant(1.0)
    y = Function(V)
    
    bc = DirichletBC(V, Constant(0.0), facet_markers, 1)
    
    # Initialization problem to get good initial guess for nonlinear problem:
    F1 = inner(grad(u), grad(v))*dx - f*v*dx
    solve(lhs(F1)==rhs(F1), y, bc)
    
    # Stabilized Eikonal equation 
    eps = Constant(mesh.hmax()/25)
    F = sqrt(inner(grad(y), grad(y)))*v*dx - f*v*dx + eps*inner(grad(y), grad(v))*dx
    solve(F==0, y, bc)
    return y

def check_boundary_velocitys(V, nodes_on_boundary):
    '''make sure the shape of the domain is not changed '''
    vtx2dof = vertex_to_dof_map(V.function_space())
    V.vector()[vtx2dof[2 * nodes_on_boundary]] = 0
    return V

# see Computer Graphics by F.S. Hill
#
def perp( a ) :
    b = empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b

# line segment a given by endpoints a1, a2
# line segment b given by endpoints b1, b2
# return 
def seg_intersect(a1,a2, b1,b2) :
    da = a2-a1
    db = b2-b1
    dp = a1-b1
    dap = perp(da)
    denom = dot( dap, db)
    num = dot( dap, dp )
    return (num / denom.astype(float))*db + b1

def get_neighbor(vertex, mesh, node_markers):
    idx = vertex.index()
    neighborhood = [Edge(mesh, i).entities(0) for i in vertex.entities(1)]
    neighborhood = array(neighborhood).flatten()

    # Remove own index from neighborhood
    neighborhood = neighborhood[where(neighborhood != idx)[0]]
    for index in neighborhood:
        if node_markers.array()[index] == 1:
            return index

def map_function_from_submesh_to_mesh(submesh, V_l, f_l, mesh, V, f):
    # map dof from submesh back to original mesh for a CG1 vector function
    map = vertex_to_dof_map(V)
    map_l = vertex_to_dof_map(V_l)
    mesh_to_mesh = submesh.data().array('parent_vertex_indices', 0).reshape(\
        int(map_l.shape[0] / 2), 1)
    mesh_to_mesh = concatenate((mesh_to_mesh * 2, \
        mesh_to_mesh * 2 + 1), axis = 1)
    mesh_to_mesh = mesh_to_mesh.flatten()
    f.vector()[map[mesh_to_mesh]] = f_l.vector()[map_l]    