# -*- coding: utf-8 -*-
"""
Set up the backward problem after forward problem is solved

Created on Wed Nov 25 13:53:00 2015

@author: baran
"""
#################################################################################
## Import required functions
#################################################################################
from dolfin import *
from Problems import Problem
from Solvers import Solver
from gradient_helpers import Phi
from matplotlib.pyplot import figure, show, pause, close

class Stefan_problem_backward():
    def __init__(self, problem):
        
        #########################################################################
        ## Load problem data
        #########################################################################
        self.adjoint = Problem("adjoint_problem", problem)
        
        #################################################################################
        ## Settings for Script
        #################################################################################
        self.plot_level = self.adjoint.problem.plot_level
        self.save_data = self.adjoint.problem.save_data
        
        #########################################################################
        ## Initialize Solvers
        #########################################################################
        # psi on interface 
        self.solver_adjoint_interface = Solver("adjoint_interface", self.adjoint)
        # adjoint temperature omega
        self.solver_adjoint_heat = Solver("adjoint_heat", self.adjoint)
        # adjoint NSE
        self.solver_adjoint_NSE = Solver("adjoint_NSE", self.adjoint)
        # phi
        self.solver_phi = Phi(self.adjoint)
        
        #########################################################################
        ## Initialize plots
        #########################################################################
        if self.plot_level:
            figure_count = 1
            if self.plot_level > 1:
                figure(figure_count)
                plot(self.adjoint.T, title="temperature at   t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                plot(self.adjoint.u, title="velocity   t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                plot(self.adjoint.V_all, title="mesh movement   t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                plot(self.adjoint.facet_markers, title="interface   t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                plot(self.adjoint.h, title="graph   t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                plot(self.adjoint.omega, title="adjoint temperature omega at  t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                plot(self.adjoint.gamma0, title="adjoint velocity gamma at  t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                figure(figure_count)
                plot(self.adjoint.pi0, title="adjoint preasure pi at  t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                show(block=False)
                pause(1e-4)
                figure_count += 1
                print("phi:")
                print(self.adjoint.phi)
            figure(figure_count)
            plot(self.adjoint.psi0, title="psi at interface   t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
            show(block=False)
            pause(1e-4)
            
        
        #########################################################################
        ## Initialize files 
        #########################################################################
        if self.save_data:
            self.a_tfile = File("results/adjoint_temperature.pvd")
            self.a_vfile = File("results/adjoint_velocity.pvd")
            self.a_int_vfile = File("results/adjoint_interface.pvd")
            self.a_pfile = File("results/adjoint_preasure.pvd")
        
    def solve(self):
        #########################################################################
        ## Start iteration
        #########################################################################
        while self.adjoint.i > 0:
            if self.plot_level:
                print('t = %1.2f s' % self.adjoint.problem.t[self.adjoint.i - 1])
            
            #####################################################################
            ## solve for psi on interface
            ##################################################################### 
            self.solver_adjoint_interface.solve(self.adjoint)
            
            #####################################################################
            ## solve for omega
            ##################################################################### 
            self.solver_adjoint_heat.solve(self.adjoint)
            
            #####################################################################
            ## solve for gamma and pi
            ##################################################################### 
            self.solver_adjoint_NSE.solve(self.adjoint)
            
            #####################################################################
            ## solve for phi
            ##################################################################### 
            self.solver_phi.solve(self.adjoint)
            
            #####################################################################
            ## Prepare next step
            #####################################################################
            self.adjoint.previous_time_step()
            
            #####################################################################
            ## Plot
            #####################################################################
            if self.plot_level:
                figure_count = 1
                if self.plot_level > 1:
                    figure(figure_count)
                    plot(self.adjoint.T, title="temperature at   t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                    show(block=False)
                    pause(1e-4)
                    figure_count += 1
                    figure(figure_count)
                    plot(self.adjoint.u, title="velocity   t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                    show(block=False)
                    pause(1e-4)
                    figure_count += 1
                    figure(figure_count)
                    plot(self.adjoint.V_all, title="mesh movement   t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                    show(block=False)
                    pause(1e-4)
                    figure_count += 1
                    figure(figure_count)
                    plot(self.adjoint.facet_markers, title="interface   t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                    show(block=False)
                    pause(1e-4)
                    figure_count += 1
                    figure(figure_count)
                    plot(self.adjoint.h, title="graph   t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                    show(block=False)
                    pause(1e-4)
                    figure_count += 1
                    figure(figure_count)
                    plot(self.adjoint.omega, title="adjoint temperature omega at  t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                    show(block=False)
                    pause(1e-4)
                    figure_count += 1
                    figure(figure_count)
                    plot(self.adjoint.gamma0, title="adjoint velocity gamma at  t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                    show(block=False)
                    pause(1e-4)
                    figure_count += 1
                    figure(figure_count)
                    plot(self.adjoint.pi0, title="adjoint preasure pi at  t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                    show(block=False)
                    pause(1e-4)
                    figure_count += 1
                    figure(figure_count)
                    print("phi:")
                    print(self.adjoint.phi)
                figure(figure_count)
                plot(self.adjoint.psi0, title="psi at interface   t = %1.2f s" % self.adjoint.problem.t[self.adjoint.i], rescale=True)
                show(block=False)
                pause(1e-4)
            if not (self.adjoint.i % 10):
                close('all')
                
            #####################################################################
            ## Save results
            #####################################################################
            if self.save_data:
                self.a_tfile << self.adjoint.omega
                self.a_vfile << self.adjoint.gamma
                self.a_int_vfile << self.adjoint.psi
                self.a_pfile << self.adjoint.pi
        
    def reset(self, problem):
        self.__init__(problem)