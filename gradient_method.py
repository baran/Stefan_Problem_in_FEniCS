# -*- coding: utf-8 -*-
"""
Gradient method for Stefan problem

Created on Tue Dec  1 08:51:50 2015

@author: baran
"""
###############################################################################
## Import 
###############################################################################
from stefan_problem_forward import Stefan_problem_forward
from stefan_problem_backward import Stefan_problem_backward
from scipy import zeros, save
from scipy.linalg import norm
from gradient_helpers import Stepsize

###############################################################################
## Parameter values
###############################################################################
max_it = 100
i = 0
update_tol = 1e-8
cost_tol = 1e-4
file_name= "Py_3_test"

###############################################################################
## Solve initial forward and backward Stefan problem
###############################################################################

###############################################################################
## Initialize forward problem
###############################################################################
forward = Stefan_problem_forward([])
control = forward.problem.p_in
save("tests/initial_guess_%s.npy" % file_name, control)
cost = zeros(max_it)
dist = zeros(max_it)
dist_all = zeros(max_it)
cost_p = zeros(max_it)

###############################################################################
## Solve forward problem
###############################################################################
forward.solve()
# save uncontrolled interface position
int_c = forward.problem.mesh.coordinates()[forward.problem.node_markers.array() != 0]
int_c = int_c[int_c[ : , 0].argsort()]
save("tests/interface_%s_uncont.npy" % file_name, int_c)
save("tests/h_mat_%s_uncont.npy" % file_name, forward.problem.h_mat)
cost[0] = forward.cost_value
dist[0] = forward.cost.dist.eval(forward.problem)
dist_all[0] = forward.cost.d2.sum()
cost_p[0] = forward.cost.control.cost
i = i + 1

###############################################################################
## Initialize backward problem
###############################################################################
backward = Stefan_problem_backward(forward.problem)

###############################################################################
## Solve backward problem
###############################################################################
backward.solve()

###############################################################################
## Initialize stepsize solver
###############################################################################
stepsize_solver = Stepsize()

###############################################################################
## Start gradient methods iteration
###############################################################################
while i < max_it:
    print("#################################################################################")
    print("## iteration step %d" % i)    
    print("#################################################################################")
    ###########################################################################
    ## Adjust control variable
    ###########################################################################
    update = - (forward.problem.lambd * forward.problem.p_in + backward.adjoint.phi)
    (stepsize, forward_solve) = stepsize_solver.get_s(forward, update, control)
    update = stepsize * update
    print("stepsize:")
    print(stepsize)
    control = stepsize_solver.project(control + update, forward.problem)
    forward.problem.p_in = control
    
    ###########################################################################
    ## Solve forward problem
    ###########################################################################
    if forward_solve:
        forward.reset(control)
        forward.solve()
    cost[i] = forward.cost_value
    dist[i] = forward.cost.dist.eval(forward.problem)
    dist_all[i] = forward.cost.d2.sum()
    cost_p[i] = forward.cost.control.cost
    ###########################################################################
    ## Evalueate stopping criterion
    ###########################################################################
    if (abs(cost[i] - cost[i - 1]) / cost[i - 1]) < cost_tol:
        print("#################################################################################")
        print("## relative change of cost: %g" % (abs(cost[i] - cost[i - 1]) / cost[i - 1]))    
        print("## stopping gradient method")    
        print("#################################################################################")
        break
    if norm(update) < update_tol:
        print("#################################################################################")
        print("## update norm: %g" % norm(update))    
        print("## stopping gradient method")    
        print("#################################################################################")
        break
    
    ###########################################################################
    ## Reset backward problem
    ###########################################################################
    backward.reset(forward.problem)
    
    ###########################################################################
    ## Solve backward problem
    ###########################################################################
    backward.solve()
    
    ###########################################################################
    ## Next step
    ###########################################################################
    i = i + 1
cost = cost[0 : i + 1]
dist = dist[0 : i + 1]
cost_p = cost_p[0 : i + 1]
dist_all = dist_all[0 : i + 1]
print(cost)
print(forward.cost.dist.eval(forward.problem))
print("control:")
print(control)
# save results
save("tests/control_%s.npy" % file_name, control)
int_c = forward.problem.mesh.coordinates()[forward.problem.node_markers.array() != 0]
int_c = int_c[int_c[ : , 0].argsort()]
save("tests/interface_%s_controlled.npy" % file_name, int_c)
save("tests/h_mat_%s_controlled.npy" % file_name, forward.problem.h_mat)
save("tests/cost_%s.npy" % file_name, cost)
save("tests/cost_p_%s.npy" % file_name, cost_p)
save("tests/dist_%s.npy" % file_name, dist)
save("tests/dist_all_%s.npy" % file_name, dist_all)
